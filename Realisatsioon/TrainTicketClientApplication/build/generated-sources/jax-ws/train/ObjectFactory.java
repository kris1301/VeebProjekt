
package train;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the train package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetTripResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "getTripResponse");
    private final static QName _AddTripResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "addTripResponse");
    private final static QName _GetFareCardResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "getFareCardResponse");
    private final static QName _AddFareCardResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "addFareCardResponse");
    private final static QName _AddFareCardTicketResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "addFareCardTicketResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: train
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTripRequest }
     * 
     */
    public GetTripRequest createGetTripRequest() {
        return new GetTripRequest();
    }

    /**
     * Create an instance of {@link TripType }
     * 
     */
    public TripType createTripType() {
        return new TripType();
    }

    /**
     * Create an instance of {@link AddTripRequest }
     * 
     */
    public AddTripRequest createAddTripRequest() {
        return new AddTripRequest();
    }

    /**
     * Create an instance of {@link GetTripListRequest }
     * 
     */
    public GetTripListRequest createGetTripListRequest() {
        return new GetTripListRequest();
    }

    /**
     * Create an instance of {@link GetTripListResponse }
     * 
     */
    public GetTripListResponse createGetTripListResponse() {
        return new GetTripListResponse();
    }

    /**
     * Create an instance of {@link GetFareCardRequest }
     * 
     */
    public GetFareCardRequest createGetFareCardRequest() {
        return new GetFareCardRequest();
    }

    /**
     * Create an instance of {@link FareCardType }
     * 
     */
    public FareCardType createFareCardType() {
        return new FareCardType();
    }

    /**
     * Create an instance of {@link AddFareCardRequest }
     * 
     */
    public AddFareCardRequest createAddFareCardRequest() {
        return new AddFareCardRequest();
    }

    /**
     * Create an instance of {@link GetFareCardListRequest }
     * 
     */
    public GetFareCardListRequest createGetFareCardListRequest() {
        return new GetFareCardListRequest();
    }

    /**
     * Create an instance of {@link GetFareCardListResponse }
     * 
     */
    public GetFareCardListResponse createGetFareCardListResponse() {
        return new GetFareCardListResponse();
    }

    /**
     * Create an instance of {@link GetFareCardTicketListRequest }
     * 
     */
    public GetFareCardTicketListRequest createGetFareCardTicketListRequest() {
        return new GetFareCardTicketListRequest();
    }

    /**
     * Create an instance of {@link GetFareCardTicketListResponse }
     * 
     */
    public GetFareCardTicketListResponse createGetFareCardTicketListResponse() {
        return new GetFareCardTicketListResponse();
    }

    /**
     * Create an instance of {@link TicketType }
     * 
     */
    public TicketType createTicketType() {
        return new TicketType();
    }

    /**
     * Create an instance of {@link AddFareCardTicketRequest }
     * 
     */
    public AddFareCardTicketRequest createAddFareCardTicketRequest() {
        return new AddFareCardTicketRequest();
    }

    /**
     * Create an instance of {@link FareCardTicketListType }
     * 
     */
    public FareCardTicketListType createFareCardTicketListType() {
        return new FareCardTicketListType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TripType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/train/1.0", name = "getTripResponse")
    public JAXBElement<TripType> createGetTripResponse(TripType value) {
        return new JAXBElement<TripType>(_GetTripResponse_QNAME, TripType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TripType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/train/1.0", name = "addTripResponse")
    public JAXBElement<TripType> createAddTripResponse(TripType value) {
        return new JAXBElement<TripType>(_AddTripResponse_QNAME, TripType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FareCardType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/train/1.0", name = "getFareCardResponse")
    public JAXBElement<FareCardType> createGetFareCardResponse(FareCardType value) {
        return new JAXBElement<FareCardType>(_GetFareCardResponse_QNAME, FareCardType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FareCardType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/train/1.0", name = "addFareCardResponse")
    public JAXBElement<FareCardType> createAddFareCardResponse(FareCardType value) {
        return new JAXBElement<FareCardType>(_AddFareCardResponse_QNAME, FareCardType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TicketType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2016/ws/train/1.0", name = "addFareCardTicketResponse")
    public JAXBElement<TicketType> createAddFareCardTicketResponse(TicketType value) {
        return new JAXBElement<TicketType>(_AddFareCardTicketResponse_QNAME, TicketType.class, null, value);
    }

}
