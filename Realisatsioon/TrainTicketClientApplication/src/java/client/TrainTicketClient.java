/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.Random;
import train.*;

public class TrainTicketClient {
    private static BigInteger myFareCardId;
    
    public static void main(String... args) {
        AddFareCardRequest fcReq = new AddFareCardRequest();
        fcReq.setToken("123");
        fcReq.setOwner("Kristjan");
        fcReq.setRequestID(generateRandom(10));
        FareCardType fc = addFareCard(fcReq);
        myFareCardId = fc.getId();
        
        AddTripRequest atripReq = new AddTripRequest();
        atripReq.setToken("123");
        atripReq.setRequestID(generateRandom(10));
        atripReq.setDepartureTime(new XMLGregorianCalendarImpl(new GregorianCalendar(2016, 12, 16, 8, 30, 00)));
        atripReq.setArrivalTime(new XMLGregorianCalendarImpl(new GregorianCalendar(2016, 12, 16, 9, 45, 00)));
        atripReq.setRoute("Rapla - Tallinn");
        atripReq.setTrainId("T72");
        
        TripType tt = addTrip(atripReq);
        
        AddFareCardTicketRequest afcReq = new AddFareCardTicketRequest();
        afcReq.setToken("123");
        afcReq.setRequestID(generateRandom(10));
        afcReq.setFareCardId(myFareCardId);
        afcReq.setTrip(tt.getId());
        afcReq.setDeparture("Rapla");
        afcReq.setDestination("Liiva");
        afcReq.setPrice(BigDecimal.valueOf(2.40));
        
        TicketType ticket = addFareCardTicket(afcReq);
        
        GetFareCardRequest facaReq = new GetFareCardRequest();
        facaReq.setToken("123");
        facaReq.setId(myFareCardId);
        
        FareCardType fareCard = getFareCard(facaReq);
        System.out.println(fareCard.getId());
        System.out.println(fareCard.getOwner());
        for (TicketType t : fareCard.getTickets().getTicket()) {
            System.out.println("departure " + t.getDeparture());
            System.out.println("destination " + t.getDestination());
            System.out.println("price " + t.getPrice());
            System.out.println("Train route " + t.getTrip().getRoute());
            System.out.println("Train departure time " + t.getTrip().getDepartureTime());
            System.out.println("Train ID " + t.getTrip().getTrainId());
        }
        
        
        
        
        
                
    }
    
    private static FareCardType addFareCard(AddFareCardRequest req) {
        TrainTicketService service = new TrainTicketService();
        TrainTicketPortType port = service.getTrainTicketPort();
        return port.addFareCard(req);
    }
    
    private static TripType addTrip(AddTripRequest req) {
        TrainTicketService service = new TrainTicketService();
        TrainTicketPortType port = service.getTrainTicketPort();
        return port.addTrip(req);
    }
    
    private static TicketType addFareCardTicket(AddFareCardTicketRequest req) {
        TrainTicketService service = new TrainTicketService();
        TrainTicketPortType port = service.getTrainTicketPort();
        return port.addFareCardTicket(req);
    }
    
    private static FareCardType getFareCard(GetFareCardRequest req) {
        TrainTicketService service = new TrainTicketService();
        TrainTicketPortType port = service.getTrainTicketPort();
        return port.getFareCard(req);
    }
    
    
    
    
    public static String generateRandom(int len) {
        char[] chars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z'};
        String r = "";
        for (int i = 0; i < len; i++) {
            r += chars[(new Random()).nextInt(chars.length)];
        }
        return r;
    }
    
}
