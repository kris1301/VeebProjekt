
package ee.ttu.idu0075._2016.ws.train._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fareCard" type="{http://www.ttu.ee/idu0075/2016/ws/train/1.0}fareCardType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareCard"
})
@XmlRootElement(name = "getFareCardListResponse")
public class GetFareCardListResponse {

    protected List<FareCardType> fareCard;

    /**
     * Gets the value of the fareCard property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareCard property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareCard().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FareCardType }
     * 
     * 
     */
    public List<FareCardType> getFareCard() {
        if (fareCard == null) {
            fareCard = new ArrayList<FareCardType>();
        }
        return this.fareCard;
    }
    
    public void setFareCard(List<FareCardType> fareCard) {
        this.fareCard = fareCard;
    }

}
