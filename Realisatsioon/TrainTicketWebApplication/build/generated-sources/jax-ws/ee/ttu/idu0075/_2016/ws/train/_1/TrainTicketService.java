
package ee.ttu.idu0075._2016.ws.train._1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.11-b150120.1832
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "TrainTicketService", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/train/1.0", wsdlLocation = "file:/C:/Users/Kristjan/Documents/NetBeansProjects/TrainTicketWebApplication/src/conf/xml-resources/web-services/TrainTicketWebService/wsdl/TrainTicketService.wsdl")
public class TrainTicketService
    extends Service
{

    private final static URL TRAINTICKETSERVICE_WSDL_LOCATION;
    private final static WebServiceException TRAINTICKETSERVICE_EXCEPTION;
    private final static QName TRAINTICKETSERVICE_QNAME = new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "TrainTicketService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/C:/Users/Kristjan/Documents/NetBeansProjects/TrainTicketWebApplication/src/conf/xml-resources/web-services/TrainTicketWebService/wsdl/TrainTicketService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        TRAINTICKETSERVICE_WSDL_LOCATION = url;
        TRAINTICKETSERVICE_EXCEPTION = e;
    }

    public TrainTicketService() {
        super(__getWsdlLocation(), TRAINTICKETSERVICE_QNAME);
    }

    public TrainTicketService(WebServiceFeature... features) {
        super(__getWsdlLocation(), TRAINTICKETSERVICE_QNAME, features);
    }

    public TrainTicketService(URL wsdlLocation) {
        super(wsdlLocation, TRAINTICKETSERVICE_QNAME);
    }

    public TrainTicketService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, TRAINTICKETSERVICE_QNAME, features);
    }

    public TrainTicketService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public TrainTicketService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns TrainTicketPortType
     */
    @WebEndpoint(name = "TrainTicketPort")
    public TrainTicketPortType getTrainTicketPort() {
        return super.getPort(new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "TrainTicketPort"), TrainTicketPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns TrainTicketPortType
     */
    @WebEndpoint(name = "TrainTicketPort")
    public TrainTicketPortType getTrainTicketPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ttu.ee/idu0075/2016/ws/train/1.0", "TrainTicketPort"), TrainTicketPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (TRAINTICKETSERVICE_EXCEPTION!= null) {
            throw TRAINTICKETSERVICE_EXCEPTION;
        }
        return TRAINTICKETSERVICE_WSDL_LOCATION;
    }

}
