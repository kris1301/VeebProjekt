package train;

import ee.ttu.idu0075._2016.ws.train._1.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class RequestBuilder {


    public static GetTripRequest getTripRequest(String token, BigInteger id) {
        GetTripRequest req = new GetTripRequest();
        req.setId(id);
        req.setToken(token);
        return req;
    }

    public static GetTripListRequest getTripListRequest(String token, XMLGregorianCalendar fromDate,
                                                        XMLGregorianCalendar toDate, String route) {
        GetTripListRequest req = new GetTripListRequest();
        req.setToken(token);
        req.setFromDate(fromDate);
        req.setToDate(toDate);
        req.setRoute(route);
        return req;
    }

    public static GetFareCardTicketListRequest getFareCardTicketListRequest(String token, BigInteger id) {
        GetFareCardTicketListRequest req = new GetFareCardTicketListRequest();
        req.setToken(token);
        req.setFareCardId(id);
        return req;
    }

    public static GetFareCardRequest getFareCardRequest(String token, BigInteger id) {
        GetFareCardRequest req = new GetFareCardRequest();
        req.setToken(token);
        req.setId(id);
        return req;
    }

    public static GetFareCardListRequest getFareCardListRequest(String token) {
        GetFareCardListRequest req = new GetFareCardListRequest();
        req.setToken(token);
        return req;
    }

    public static XMLGregorianCalendar convertDate(String dateString) throws ParseException, DatatypeConfigurationException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date date = formatter.parse(dateString);
        GregorianCalendar gregory = new GregorianCalendar();
        gregory.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
    }
}
