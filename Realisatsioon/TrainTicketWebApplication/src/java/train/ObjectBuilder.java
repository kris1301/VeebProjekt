package train;


import ee.ttu.idu0075._2016.ws.train._1.*;
import javax.xml.datatype.DatatypeConstants;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ObjectBuilder {

    public static TripType createTripTypeFromRequest(AddTripRequest parameter) {
        TripType t = new TripType();
        t.setId(Data.tripId);
        Data.tripId = Data.tripId.add(BigInteger.ONE);
        t.setTrainId(parameter.getTrainId());
        t.setRoute(parameter.getRoute());
        t.setDepartureTime(parameter.getDepartureTime());
        t.setArrivalTime(parameter.getArrivalTime());
        if (t.getDepartureTime().compare(t.getArrivalTime()) == DatatypeConstants.GREATER) {
            throw new IllegalArgumentException("Departure time can not be after arrival time.");
        }
        Data.tripMap.put(t.getId(), t);
        Data.trips.add(t);
        return t;
    }

    public static GetTripListResponse createGetTripListFromRequest(GetTripListRequest parameter) {
        GetTripListResponse t = new GetTripListResponse();
        Predicate<TripType> start = x -> true;

        if (parameter.getFromDate() != null) {
            start = x -> x.getDepartureTime().compare(parameter.getFromDate()) != DatatypeConstants.LESSER;
        }
        Predicate<TripType> end = x -> true;
        if (parameter.getToDate() != null) {
            end = x -> x.getArrivalTime().compare(parameter.getToDate()) != DatatypeConstants.GREATER;
        }
        Predicate<TripType> route = x -> true;
        if (parameter.getRoute() != null) {

            route = x -> x.getRoute().equals(parameter.getRoute());
        }
        t.setTrip(Data.trips.stream().filter(start).filter(end).filter(route).collect(Collectors.toList()));
        return t;
    }

    public static FareCardType getFareCard(GetFareCardRequest parameter) {
        if (Data.fareCardMap.containsKey(parameter.getId())) {
            return Data.fareCardMap.get(parameter.getId());
        }
        return null;
    }

    public static FareCardType createFareCardType(AddFareCardRequest parameter) {
        FareCardType f = new FareCardType();
        f.setId(Data.fareCardId);
        f.setOwner(parameter.getOwner());
        Data.fareCardId = Data.fareCardId.add(BigInteger.ONE);
        Data.fareCardMap.put(f.getId(), f);
        Data.fareCards.add(f);
        return f;
    }

    public static GetFareCardListResponse createGetFareCardList(GetFareCardListRequest parameter) {
        GetFareCardListResponse g = new GetFareCardListResponse();
        g.setFareCard(Data.fareCards);
        return g;
    }

    public static GetFareCardTicketListResponse getFareCardTicketList(GetFareCardTicketListRequest parameter) {
        if (!Data.fareCardMap.containsKey(parameter.getFareCardId())) {
            return null;
        }
        GetFareCardTicketListResponse g = new GetFareCardTicketListResponse();
        g.setTicket(Data.fareCardMap.get(parameter.getFareCardId()).getTickets().getTicket());;
        return g;
    }

    public static TicketType createTicketType(AddFareCardTicketRequest parameter) {
        TicketType t = new TicketType();
        t.setDeparture(parameter.getDeparture());
        t.setDestination(parameter.getDestination());
        t.setPrice(parameter.getPrice());
        t.setTrip(Data.tripMap.get(parameter.getTrip()));
        return t;

    }

    public static void addTicketToFareCard(BigInteger fareCardId, TicketType t) {
        if (Data.fareCardMap.containsKey(fareCardId)) {
            if (Data.fareCardMap.get(fareCardId).getTickets() == null) {
                Data.fareCardMap.get(fareCardId).setTickets(new FareCardTicketListType());
                Data.fareCardMap.get(fareCardId).getTickets().setTicket(new ArrayList<>());;
            }
            Data.fareCardMap.get(fareCardId).getTickets().getTicket().add(t);
        }
    }
}
