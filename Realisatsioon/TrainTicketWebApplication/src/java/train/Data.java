package train;


import ee.ttu.idu0075._2016.ws.train._1.FareCardType;
import ee.ttu.idu0075._2016.ws.train._1.TicketType;
import ee.ttu.idu0075._2016.ws.train._1.TripType;


import java.math.BigInteger;
import java.util.*;

public class Data {
    public static List<FareCardType> fareCards = new ArrayList<>();
    public static Map<BigInteger, FareCardType> fareCardMap = new HashMap<>();

    public static List<TripType> trips = new ArrayList<>();
    public static Map<BigInteger, TripType> tripMap = new HashMap<>();

    public static BigInteger tripId = BigInteger.ONE;
    public static BigInteger fareCardId = BigInteger.ONE;

    public static Set<String> reqIDsTrip = new HashSet<>();
    public static Map<String, BigInteger> tripIdMap = new HashMap<>();

    public static Set<String> reqIdsFareCard = new HashSet<>();
    public static Map<String, BigInteger> fareCardIdMap = new HashMap<>();
    public static Set<String> reqIdsFareTickets = new HashSet<>();
    public static Map<String, TicketType> fareTicketsMap = new HashMap<>();

}
