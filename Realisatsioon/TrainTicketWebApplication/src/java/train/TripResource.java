/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package train;

import ee.ttu.idu0075._2016.ws.train._1.GetTripListResponse;
import ee.ttu.idu0075._2016.ws.train._1.*;
import java.math.BigInteger;
import java.text.ParseException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.*;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * REST Web Service
 *
 * @author Kristjan
 */
@Path("trip")
public class TripResource {
    TrainTicketWebService ts = new TrainTicketWebService();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TripResource
     */
    public TripResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetTripListResponse getTripList(@QueryParam("token") String token,
                                           @QueryParam("fromDate") String from,
                                           @QueryParam("toDate") String to,
                                           @QueryParam("route") String route) {
        XMLGregorianCalendar fromDate = null;
        if (from != null) {
            try {
                fromDate = RequestBuilder.convertDate(from);
            } catch (ParseException | DatatypeConfigurationException e) {
                throw new IllegalArgumentException("Illegal date format.");
            }
        }

        XMLGregorianCalendar toDate = null;
        if (to != null) {
            try {
                toDate = RequestBuilder.convertDate(to);
            } catch (ParseException | DatatypeConfigurationException e) {
                e.printStackTrace();
            }
        }
        GetTripListRequest req = RequestBuilder.getTripListRequest(token, fromDate, toDate, route);
        return ts.getTripList(req);
    }

    @GET
    @Path("{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public TripType getTrip(@PathParam("id") String id,
                            @QueryParam("token") String token) {
        GetTripRequest req = RequestBuilder.getTripRequest(token, BigInteger.valueOf(Integer.parseInt(id)));
        return ts.getTrip(req);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TripType addTrip(AddTripRequest content,
                                  @QueryParam("token") String token) {
        content.setToken(token);
        return ts.addTrip(content);
    }
}
