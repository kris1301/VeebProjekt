/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package train;

import ee.ttu.idu0075._2016.ws.train._1.*;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Kristjan
 */
@Path("fareCard")
public class FareCardResource {
    TrainTicketWebService ts = new TrainTicketWebService();

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of FareCardResource
     */
    public FareCardResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetFareCardListResponse getFareCardList(@QueryParam("token") String token) {
        GetFareCardListRequest req = RequestBuilder.getFareCardListRequest(token);
        return ts.getFareCardList(req);
    }

    @GET
    @Path("{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public FareCardType getFareCard(@PathParam("id") String id, @QueryParam("token") String token) {
        GetFareCardRequest req = RequestBuilder.getFareCardRequest(token, BigInteger.valueOf(Integer.parseInt(id)));
        return ts.getFareCard(req);
    }

    @GET
    @Path("{id : \\d+}/ticket")
    @Produces(MediaType.APPLICATION_JSON)
    public GetFareCardTicketListResponse getFareCardTicketList(@PathParam("id") String id, @QueryParam("token") String token) {
        GetFareCardTicketListRequest req = RequestBuilder.getFareCardTicketListRequest(token, BigInteger.valueOf(Integer.parseInt(id)));
        return ts.getFareCardTicketList(req);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public FareCardType addFareCard(AddFareCardRequest content, @QueryParam("token") String token) {
        content.setToken(token);
        return ts.addFareCard(content);
    }


    @POST
    @Path("{id : \\d+}/ticket")
    @Produces("application/json")
    @Consumes("application/json")
    public TicketType addFareCardTicket(AddFareCardTicketRequest content, @QueryParam("token") String token, @PathParam("id") String id) {
        content.setToken(token);
        content.setFareCardId(BigInteger.valueOf(Integer.parseInt(id)));
        return ts.addFareCardTicket(content);
    }
}
