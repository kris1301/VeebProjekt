/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package train;

import com.sun.xml.ws.developer.SchemaValidation;
import ee.ttu.idu0075._2016.ws.train._1.*;
import java.util.Objects;
import javax.jws.WebService;

/**
 *
 * @author Kristjan
 */
@SchemaValidation(handler=MyErrorHandler.class)
@WebService(serviceName = "TrainTicketService", portName = "TrainTicketPort", endpointInterface = "ee.ttu.idu0075._2016.ws.train._1.TrainTicketPortType", targetNamespace = "http://www.ttu.ee/idu0075/2016/ws/train/1.0", wsdlLocation = "WEB-INF/wsdl/TrainTicketWebService/TrainTicketService.wsdl")
public class TrainTicketWebService {

    private String VALID_API_TOKEN = "123";

    public TripType getTrip(GetTripRequest parameter) {
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        if (Data.tripMap.containsKey(parameter.getId())) {
            return Data.tripMap.get(parameter.getId());
        }
        return null;
    }

    public TripType addTrip(AddTripRequest parameter) {
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        String rid = parameter.getRequestID() + "::" + parameter.getToken();
        if (Data.reqIDsTrip.contains(rid)) {
            return Data.tripMap.get(Data.tripIdMap.get(rid));
        }
        TripType t = ObjectBuilder.createTripTypeFromRequest(parameter);
        Data.reqIDsTrip.add(rid);
        Data.tripIdMap.put(rid, t.getId());
        return t;
    }

    public GetTripListResponse getTripList(GetTripListRequest parameter) {
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        return ObjectBuilder.createGetTripListFromRequest(parameter);
    }

    public FareCardType getFareCard(GetFareCardRequest parameter) {
        System.out.println("fc" + parameter.getId());
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        return ObjectBuilder.getFareCard(parameter);
    }

    public FareCardType addFareCard(AddFareCardRequest parameter) {
        System.out.println("REQUESTID");
        System.out.println(parameter.getRequestID());
        System.out.println("---");
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        String rid = parameter.getRequestID() + "::" + parameter.getToken();
        if (Data.reqIdsFareCard.contains(rid)) {
            return Data.fareCardMap.get(Data.fareCardIdMap.get(rid));
        }
        FareCardType t = ObjectBuilder.createFareCardType(parameter);
        Data.reqIdsFareCard.add(rid);
        Data.fareCardIdMap.put(rid, t.getId());
        return t;
    }

    public GetFareCardListResponse getFareCardList(GetFareCardListRequest parameter) {
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        return ObjectBuilder.createGetFareCardList(parameter);
    }

    public GetFareCardTicketListResponse getFareCardTicketList(GetFareCardTicketListRequest parameter) {
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        return ObjectBuilder.getFareCardTicketList(parameter);
    }

    public TicketType addFareCardTicket(AddFareCardTicketRequest parameter) {
        if (!Objects.equals(parameter.getToken(), VALID_API_TOKEN)) {
            throw new IllegalArgumentException("Invalid API token.");
        }
        if (!Data.fareCardMap.containsKey(parameter.getFareCardId())) {
            throw new IllegalArgumentException("Invalid Fare Card ID");
        }
        if (!Data.tripMap.containsKey(parameter.getTrip())) {
            throw new IllegalArgumentException("Invalid Trip ID");
        }
        String rid = parameter.getRequestID() + "::" + parameter.getToken();
        System.out.println(rid);
        if (Data.reqIdsFareTickets.contains(rid)) {
            return Data.fareTicketsMap.get(rid);
        }
        TicketType t = ObjectBuilder.createTicketType(parameter);
        Data.reqIdsFareTickets.add(rid);
        Data.fareTicketsMap.put(rid, t);
        ObjectBuilder.addTicketToFareCard(parameter.getFareCardId(), t);
        return t;

    }
    
}
